package Subiectul2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Interface extends JFrame {
    JButton button1;
    JTextField textField1;
    JTextField textField2;

    public Interface() {
        setTitle("Examen");
        setLayout(null);
        setVisible(true);
        init();
        setSize(500, 500);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void init() {

        textField1 = new JTextField();
        textField1.setBounds(100, 50, 300, 50);
        add(textField1);
        textField2 = new JTextField();
        textField2.setBounds(100, 250, 100, 50);
        add(textField2);

        button1 = new JButton("Press");
        button1.setBounds(100, 150, 100, 50);
        add(button1);
        button1.addActionListener(new B());

    }

    public static void main(String[] args) {
        new Interface();
    }




    public class B implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String s=textField1.getText();
            String words[]=s.split("\\s");
            String str="a";
            String repeted=str.repeat(s.length());
            textField2.setText(repeted);
        }



    }
}
