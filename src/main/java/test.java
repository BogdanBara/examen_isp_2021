public class test {

    String color;
    double radius;

    public test(){
        this.color = "red";
        this.radius = 1.0;
    }

    public test(double radius){
        this.radius = radius;
    }

    public double getRadius(){

        return radius;
    }

    public double getArea(){

        return Math.PI*(Math.pow(getRadius(), 2));

    }
}
